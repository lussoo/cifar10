Cifar10

Trenirani modeli od AlexNeta/VGGNetasu spremljeni su unutar direktorija (alexNet/train)/(vgg/train).

Evaluacija modela:
evaltrain.py - evaluacija na 50 000 trening slika.
evaltest.py - evaluacija na 10 000 test slika.

Vizualni pregled rezultata sa Tensorboardom:
tensorboard --logdir=train.
tensorboard --logdir=eval.

Treniranje novog modela:
train.py/vggTrain.py - učenje AlexNet/VGGNet mreže(~3h na Tesla K40 za 50k iteracija).

Arhitekture mreža su implementirane pomoću predloška sa službene Tensorflow stranice. Predložak ima implementiran sustav za data augmentation i sustav za evidenciju Tensorflow elemenata preko Tensorboarda.

Definicije mreža nalaze se unutar funkcije inference u skripti cifar10.py.
